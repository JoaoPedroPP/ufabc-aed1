#include <stdio.h>
#include <stdlib.h>

typedef struct Node {
	int key;
	struct Node *right;
	struct Node *left;
	struct Node *father;
} Node;

typedef struct Root {
	int amount;
	Node *root;
} Root;

typedef struct tree { //Nao vejo necessidade disso ainda
	Root *tree;
} tree;

void insertNode(Root*, int);
Root* initRoot();
Node* initNode(int);
int searchNode(Node*, int);
void putNode(Node*, Node*);
void showTree(Node*);
void maxValue(Node*);
void minValue(Node*);
void showLeafs(Node*);
int getHeight(Node*);

int main()
{
	int N, i, n;
	scanf("%d\n", &N);
	if (N <= 500 && N >= 1) {
		Root *r = initRoot();

		for (i = 0; i < N; i++) {
			scanf("%d\n", &n);
			insertNode(r, n);
				// printf("--------Inicio--------\n");
				// showTree(r->root);
				// printf("--------Final--------\n");
			// printf("%d\n", n);
		}
		//showLeafs(r->root);
		printf("%d\n", getHeight(r->root));

		free(r);
	}


	return 0;
}

Root* initRoot()
{
	Root *r = (Root*)malloc(sizeof(Root));
	r->amount = 0;
	r->root = NULL;

	return r;
}
Node* initNode(int key)
{
	Node *n = (Node*)malloc(sizeof(Node));
	n->key = key;
	n->right = NULL;
	n->left = NULL;
	n->father = NULL;

	return n;
}

void showTree(Node *n)
{
	if (n == NULL) {
		return;
	}
	else {
		printf("%d\n", n->key);
		showTree(n->left);
		showTree(n->right);
	}
}

int searchNode(Node *node, int key)
{
	int result;
	// printf("segunda %d\n", key);
	if (node == NULL) {
		result = 0;
	}
	else if (key < node->key) {
		result = searchNode(node->left, key);
	}
	else if (key > node->key) {
		result = searchNode(node->right, key);
	}
	else {
		// printf("%d\n", node->key);
		result = 1;
	}
	
	return result;
}

void putNode(Node *father, Node *son)
{
	if (father->key > son->key) {
		if (father->left == NULL) {
			father->left = son;
			son->father = father;
		}
		else {
			putNode(father->left, son);
		}
	}
	else if (father->key < son->key) {
		if (father->right == NULL) {
			father->right = son;
			son->father = father;
		}
		else {
			putNode(father->right, son);
		}
	}
}

void insertNode(Root *r, int key)
{
	Node *n = initNode(key);
	if (r->amount == 0) {
		r->root = n;
		r->amount++;
	}
	else {
		if(!searchNode(r->root, key)) {
//			printf("achou\n");
			// printf("Chave %d ja existe na arvore!\n", key);
		// }
		// else {
			putNode(r->root, n); //No da raiz e no a inserir
			r->amount++;
//			printf("errouuuuu\n");
		}
	}
}

void maxValue(Node *n)
{
	n->right == NULL ? printf("%d\n", n->key):maxValue(n->right);
}

void minValue(Node *n)
{
	n->left == NULL ? printf("%d\n", n->key):minValue(n->left);
}

void showLeafs(Node *n)
{
	if (n == NULL) {
		return;
	}
	else if (n->left == NULL && n->right == NULL) {
		printf("%d\n", n->key);
		return;
	}
	else {
		showLeafs(n->left);
		showLeafs(n->right);
	}
}

int getHeight(Node *n)
{
	int hR;
	int hL;

	if (n == NULL) {
		return 0;
	}
	else if (n->left == NULL && n->right == NULL) {
		//printf("%d\n", n->key);
		return 0;
	}
	else {
		hL = getHeight(n->left) + 1;
		hR = getHeight(n->right) + 1;

		return hL >= hR ? hL:hR;
	}
}
