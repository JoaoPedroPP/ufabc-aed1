#include <stdio.h>
#include <stdlib.h>

typedef struct node {
	int key;
	struct node *R;
	struct node *L;
	struct node *dad;
} node;

typedef struct root {
	node *root;
} root;

node* initiNode(int);
root* initiRoot();
void insertNode(root*, int);
node* getRight(node*);
node* getLeft(node*);
void showTree(node*);

int main()
{
	int N, i, n;
	scanf("%d\n", &N);

	root *r = initiRoot();

	for (i = 0; i < N; i++) {
		scanf("%d\n", &n);
		if (n <= 500 && n >= 1) {
			insertNode(r, n);
		}
		// printf("%d\n", n);
	}
//	showTree(r->root);

	free(r);

	return 0;
}

root* initiRoot()
{
	root *r = (root*)malloc(sizeof(root));
	r->root = NULL;

	return r;
}

node* initiNode(int key)
{
	node *n = (node*)malloc(sizeof(node));
	n->key = key;
	n->R = NULL;
	n->L = NULL;
	n->dad = NULL;	

	return n;
}

void insertNode(root *r, int key)
{
	int rep = 0;
	node *n, *n_dad, *son;
	// Buscar posicao de insercao
	if (r->root == NULL) {// primeiro elemento a ser inserido
		n = initiNode(key);
		r->root = n;
	}
	else {
		// Devo percorrer a arvore ate encontrar a posicao
		n = initiNode(key);
		son = r->root;
		while (son != NULL && rep == 0) {
			if (son->key < n->key) {// Significa que devo optar pelo ramo direito
				n_dad = son;
				son = getRight(n_dad);
			} else if (son->key > n->key) {// Significa que devo optar pelo ramo esquerdo
				n_dad = son;
				son = getLeft(n_dad);
			} else {
				rep++;
			}
		}
		if (rep == 0) {//Aqui eu ja tenho a posicao correta, preciso verificar se e reptido ou nao
			if (son->key < n->key) {// Significa que devo optar pelo ramo direito
				n_dad->R = n;
			} else if (son->key > n->key) {// Significa que devo optar pelo ramo esquerdo
				n_dad->L = n;
			}
			n->dad = n_dad;
		}
		else {
			printf("Chave %d ja existe na arvore!\n", n->key);
			free(n);
		}
	}
}

node* getRight(node *n)
{
	return n->R;
}

node* getLeft(node *n)
{
	return n->L;
}

void showTree(node *n)
{
	while (n != NULL) {
		printf("%d\n", n->key);
		showTree(n->R);
		showTree(n->L);
	}
}
