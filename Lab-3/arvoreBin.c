#include<stdio.h>
#include<stdlib.h>

/*

(slides Monael: Arvore Binaria de Busca)

Funcoes:

struct tArvore * criaArvore(void);
void lerNo(struct tNo *);
void criaRaiz(struct tArvore *);
struct tNo * criaNo(void);
struct tNo * pai(struct tNo *);
struct tNo * filhoDireita(struct tNo *);
struct tNo * filhoEsquerda(struct tNo *);
struct tNo * antecessor(struct tNo *);
struct tNo* sucessor(struct tNo *);

int ehFilhoDireita(struct tNo *);
int ehFilhoEsquerda(struct tNo *);
void insere(struct tArvore *);
void remover(struct tArvore *);
void preOrdem(struct tNo *);
void inOrdem(struct tNo *);
void posOrdem(struct tNo *);
void listar(struct tArvore *);

*/

// Implementacao da Estrutura do No
struct tNo {
    int chave;
    struct tNo *pai, *esquerda, *direita;
};

// Implementacao da Estrutura da Arvore
struct tArvore {
    struct tNo *raiz;
    int quantidade;
};

// Inicializacao da Arvore
struct tArvore * criaArvore(void) {
    struct tArvore *arvore = (struct tArvore *) malloc(sizeof(struct tArvore));
    if(arvore != NULL) {
        arvore->raiz = NULL;
        arvore->quantidade = 0;
    }
    else {
        printf("\nDificuldade na alocacao de memoria!\n");
        system("pause");
    }
    return arvore;
}

// Leitura do No
void lerNo(struct tNo *no) {
    printf("\nInforme o valor da chave: \n");
    scanf("%d", &no->chave);
    /*ler demais campos da estrutura aqui!*/
}

// Inicializacao do No
//struct tNo * criaNo(void) {
struct tNo * criaNo(int chave) {
    struct tNo *no = (struct tNo *) malloc(sizeof(struct tNo));
    if(no != NULL) {
        no->chave = chave;//lerNo(no);
        no->pai = NULL;
        no->direita = NULL;
        no->esquerda = NULL;
    }
    else {
        printf("\nDificuldade na alocacao de memoria!\n");
        system("pause");
    }
    return no;
}

// Inicializacao do Raiz
//void criaRaiz(struct tArvore *arvore) {
void criaRaiz(struct tArvore *arvore, int chave) {
    //struct tNo *raiz = criaNo();
    struct tNo *raiz = criaNo(chave);
    if(raiz != NULL) {
        arvore->raiz = raiz;
        arvore->quantidade ++;
    }
}

// Devolve o pai de um n�
struct tNo * pai(struct tNo *filho) {
    return filho->pai;
}

// Devolve o Filho Direito
struct tNo * filhoDireita(struct tNo *pai) {
    return pai->direita;
}

// Devolve o Filho Esquerdo
struct tNo * filhoEsquerda(struct tNo *pai) {
    return pai->esquerda;
}

// Verifica se � um filho da direita 
int ehFilhoDireita(struct tNo *no) {
    struct tNo *no_pai = pai(no);
    if(no_pai == NULL) {
        return 0;
    }
    else {
        if(filhoDireita(no_pai) == no) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

// Verifica se � um filho da esquerda
int ehFilhoEsquerda(struct tNo *no) {
    struct tNo *no_pai = pai(no);
    if(no_pai == NULL) {
        return 0;
    }
    else {
        if(filhoEsquerda(no_pai) == no) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

// Visita Pre-Ordem
void preOrdem(struct tNo* raiz) {
    if(raiz != NULL) {
        printf("%d ", raiz->chave);
        preOrdem(raiz->esquerda);
        preOrdem(raiz->direita);
    }
}

// Visita In-Ordem
void inOrdem(struct tNo* raiz) {
    if(raiz != NULL) {
        inOrdem(raiz->esquerda);
        printf("%d ", raiz->chave);
        inOrdem(raiz->direita);
    }
}

// Visita Pos-Ordem
void posOrdem(struct tNo* raiz) {
    if(raiz != NULL) {
        posOrdem(raiz->esquerda);
        posOrdem(raiz->direita);
        printf("\n%d\n", raiz->chave);
    }
}

// Imprime elementos da �rvore
void listar(struct tArvore *arvore) {
    if(arvore->raiz != NULL) {
        /* invoque alguma das fun��es para percorrer �rvore aqui*/
        //preOrdem(arvore->raiz);
        inOrdem(arvore->raiz);
        //posOrdem(arvore->raiz);
        printf("\n");
    }
    else {
        printf("\nA arvore estah vazia!!!\n");
        system("pause");
    }
}

// Insercao de um novo elemento na arvore
//void insere(struct tArvore *arvore) {
void insere(struct tArvore *arvore, int chave) {
    struct tNo *no, *no_pai, *filho;
    int repetido = 0;
    if(arvore->raiz == NULL) {
        criaRaiz(arvore, chave);//criaRaiz(arvore);
    }
    else {
        no = criaNo(chave); //no = criaNo();
        filho = arvore->raiz;
        
        // Busca posicao de insercao
        while(filho!=NULL && repetido==0) {
            if(filho->chave < no->chave) {
                no_pai = filho;
                filho = filhoDireita(no_pai);
            }
            else if(filho->chave > no->chave) {
                no_pai = filho;
                filho = filhoEsquerda(no_pai);
            }
            else {
                repetido = 1;
            }
        }
        
        // Insere novo elemento
        if(repetido == 0) {
            if(no_pai->chave < no->chave) {
                no_pai->direita = no;
            }
            else if(no_pai->chave > no->chave) {
                no_pai->esquerda = no;
            }
            no->pai = no_pai;
            arvore->quantidade ++;
        }
        else {
            printf("\nChave %d repetida na arvore!\n", chave);
            system("pause");
        }
    }
}

// Busca o Antecessor do n� (fesqrem = ?)
struct tNo * antecessor(struct tNo *fesqrem) {
    struct tNo *ant, *pt = fesqrem;
    
    // Descendo � direita na sub�rvore esquerda, at� localizar o antecessor.
    while(pt != NULL) {
        ant = pt;
        pt = pt->direita;
    }
    
    // Ao encontrar o antecessor, verificar se ele tem filho esquerdo. Se ele tiver.
    if(filhoEsquerda(ant) != NULL) {
    
        // Se o antecessor for filho esquerdo do seu pai, ent�o o campo filho da
        // esquerda do v� passa a apontar para o neto.
        if(ehFilhoEsquerda(ant)==1) {
            pai(ant)->esquerda = ant->esquerda;
        }
        
        else {
            pai(ant)->direita = ant->esquerda;
        }
        (ant->esquerda)->pai = pai(ant);
    }
    // Ao encontrar o antecessor, verificar se ele tem filho esquerdo. 
    // Se ele n�o tiver, ent�o ele � uma folha.
    else {
        if(ehFilhoEsquerda(ant)==1) {
            pai(ant)->esquerda = NULL;
        }
        else {
            pai(ant)->direita = NULL;
        }
    }
    return ant;
}

// Busca o Sucessor do n�.
struct tNo * sucessor(struct tNo *fdirrem) {
    struct tNo *suc, *pt = fdirrem;
    
    // Descendo � esquerda na sub�rvore direita, at� localizar o sucessor.
    while(pt != NULL) {
        suc = pt;
        pt = pt->esquerda;
    }
    
    // Ao encontrar o sucessor, verificar se ele tem filho direito. Se ele tiver.
    if(filhoDireita(suc) != NULL) {
    
        // Se o sucessor for filho Direito do seu pai, ent�o o campo filho
        // da direita do v� passa a apontar para o neto.
        if(ehFilhoDireita(suc)==1) {
            pai(suc)->direita = suc->direita;
        }
        else {
            pai(suc)->esquerda = suc->direita;
        }
        (suc->direita)->pai = pai(suc);
    }
    
    // Ao encontrar o sucessor, verificar se ele tem filho direito.
    // Se ele n�o tiver, ent�o ele � uma folha.
    else {
        if(ehFilhoDireita(suc)==1) {
            pai(suc)->direita = NULL;
        }
        else {
            pai(suc)->esquerda = NULL;
        }
    }
    return suc;
}

/*

No: 50, Antecessor: 35, Sucessor: 55
fesqrem: 20, fdirrem: 70

                 50               
             /        \ 
          20            70
         /  \          /  \
       10   30       60    80
           /  \     /  \
         25  [35](55)   65        
             /     \
           33       57
*/


int main (int argc, char **argv) {

    struct tArvore *arv = criaArvore();

    insere(arv, 50);
    insere(arv, 20);
    insere(arv, 70);
    insere(arv, 10);
    insere(arv, 30);
    insere(arv, 60);
    insere(arv, 80);
    insere(arv, 25);
    insere(arv, 35);
    insere(arv, 55);
    insere(arv, 65);
    insere(arv, 33);
    insere(arv, 57);
    listar(arv);
    
    return 0;
}
