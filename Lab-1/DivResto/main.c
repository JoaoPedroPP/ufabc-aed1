#include <stdio.h>

int main(){
    
    int a, b, result, resto;
    
    scanf("%d %d", &a, &b);
    
    if(a == 0){
        result = 0;
        resto = 0;
    }
    else{
        result = a/b;
        resto = a%b;
    }
    
    printf("%d %d\n",   result, resto);
    
    return 0;
}