#include <stdio.h>

int main(){
    int total, seg, min, hr;
    
    scanf("%d", &total);
    
    hr = total/3600;
    min = (total%3600)/60;
    seg = total%60;
    
    printf("%d:%d:%d\n", hr, min, seg);
    
    return 0;
}