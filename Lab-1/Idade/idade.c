#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(){
    int anoAtual, anoNascimento, idade;
    char nome[100];
    
    scanf("%d %s %d", &anoAtual, nome, &anoNascimento);
    
    idade = anoAtual - anoNascimento;
    
    printf("%s, voce completa %d anos de idade neste ano.\n", nome, idade);
    
    return 0;
}