#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Item {
    char nome[100];
    int quantidade;
    struct Item *anterior, *proximo;
} Item;

typedef struct Lista {
    Item *primeiro, *ultimo;
} Lista;

Lista* inicializaLista();
Item* inicializaItem(char*);
void insereItem(Lista*, Item*);
void showLista(Lista);

int main(int agrc, char **argv)
{
    char input[100];
    Lista *l = inicializaLista();

    while (scanf("%s\n", input) != EOF) {
        // printf("%s\n", input);
        insereItem(l, inicializaItem(input));
    }
    showLista(*l);
    free(l);
}

Lista* inicializaLista()
{
    Lista *l = (Lista*)malloc(sizeof(Lista));
    l->primeiro = NULL;
    l->ultimo = NULL;

    return l;
}

Item* inicializaItem(char *nome)
{
    Item *it = (Item*)malloc(sizeof(Item));
    strcpy(it->nome, nome);
    it->quantidade = 1;
    it->anterior = NULL;
    it->proximo = NULL;

    return it;
}

void showLista(Lista l)
{
    Item *counter = l.primeiro;
    while (counter != NULL) {
        printf("%s [%d]\n", counter->nome, counter->quantidade);
        counter = counter->proximo;
    }
}

void insereItem(Lista *l, Item *it)
{
    Item *counter = l->primeiro;
    int flag_quantidade = 0;
    // printf("%s\n", it->nome);
    while (counter != NULL) {
        if (!strcmp(counter->nome, it->nome)) {
            counter->quantidade++;
            free(it);
            flag_quantidade = 1;
            // return;
        }
        counter = counter->proximo;
    }
    if (!flag_quantidade) {
        if (l->primeiro == NULL) { // Inserção do primeiro item
            l->primeiro = it;
            l->ultimo = it;
        }

        else { // Inserção dos itens no final da lista
            l->ultimo->proximo = it;
            it->anterior = l->ultimo;
            l->ultimo = it;
        }
    }

    // Nesse momento tenho lista desordenada;
    // O que tenho que fazer agora e percorrer os elementos e comparalos um a um

    Item *counter_fixo = l->primeiro;
    Item *counter_variavel = NULL;
    while (counter_fixo != NULL) {
        counter_variavel = counter_fixo;
        while (counter_variavel != NULL) {
            if (counter_variavel->quantidade > counter_fixo->quantidade) {
                printf("----------I----------\n");
                showLista(*l);
                printf("----------F----------\n");

                // Caso: Trocar primeiro com o ultimo sem ser conseguinte
                if (counter_fixo->anterior == NULL && counter_variavel->proximo == NULL && counter_fixo->proximo != counter_variavel) {
                    l->primeiro = counter_variavel;
                    l->ultimo = counter_variavel->anterior;

                    counter_variavel->anterior->proximo = NULL;
                    counter_variavel->proximo = counter_fixo;
                    counter_fixo->anterior = counter_variavel;
                    counter_variavel->anterior = NULL;
                }

                // Caso: Trocar o primeiro com o ultimo pois so tem dois elementos na lista
                else if (counter_fixo->anterior == NULL && counter_variavel->proximo == NULL && counter_fixo->proximo == counter_variavel) {
                    l->primeiro = counter_variavel;
                    l->ultimo = counter_fixo;

                    counter_variavel->proximo = counter_fixo;
                    counter_fixo->anterior = counter_variavel;
                    counter_fixo->proximo = NULL;
                    counter_variavel->anterior = NULL;

                }

                // Caso: Trocar primeiro item com algum do meio
                else if (counter_fixo->anterior == NULL && counter_variavel->proximo != NULL) {
                    counter_variavel->anterior->proximo = counter_variavel->proximo;
                    counter_variavel->proximo->anterior = counter_variavel->anterior;

                    counter_variavel->proximo = counter_fixo;
                    counter_fixo->anterior = counter_variavel;
                    counter_variavel->anterior = NULL;

                    l->primeiro = counter_variavel;
                }

                //Caso: Trocar item do meio com um do meio sem ser conseguinte
                else if (counter_fixo->anterior != NULL && counter_variavel->proximo != NULL) {
                    counter_variavel->anterior->proximo = counter_variavel->proximo;
                    counter_variavel->proximo->anterior = counter_variavel->anterior;
                    counter_fixo->anterior->proximo = counter_variavel;

                    counter_variavel->proximo = counter_fixo;
                    counter_variavel->anterior = counter_fixo->anterior;
                    counter_fixo->anterior = counter_variavel;
                }

                // Caso: Trocar item do meio com o ultimo
                else if (counter_fixo->anterior != NULL && counter_variavel->proximo == NULL) {
                    l->ultimo = counter_variavel->anterior;
                    if (counter_fixo->proximo == counter_variavel) {
                        counter_fixo->proximo = NULL;
                    }
                    else {
                        counter_variavel->anterior->proximo = NULL;
                    }

                    counter_fixo->anterior->proximo = counter_variavel;
                    counter_variavel->proximo = counter_fixo;
                    counter_variavel->anterior = counter_fixo->anterior;
                    counter_fixo->anterior = counter_variavel;
                }

                else {
                    printf("Alguma condição não implementada\n");
                }
                printf("----------I2----------\n");
                showLista(*l);
                printf("----------F2----------\n");

            }
            counter_variavel = counter_variavel->proximo;
        }
        counter_fixo = counter_fixo->proximo;
    }
    // printf("----------I----------\n");
    // showLista(*l);
    // printf("----------F----------\n");
    // showLista(*l);
}