/*

Um novo site de busca chamado Ong (Ong not google) armazena todas as
palavras procuradas em uma lista duplamente encadeada. A referida lista eh
mantida ordenada pelo ranking de consultas das expressoes, ou seja, a expressao
mais procurada eh a primeira da lista. Alem disso, cada vez que uma expressao eh
consultada no site, se ela nao consta da lista ela eh acrescentada ao final da lista.
Por outro lado, se ela eh encontrada, e a sua quantidade de consultas ultrapassa
que esta a sua frente, entao ela deve "ganhar uma ou varias posicoes" na lista,
ou seja, deve trocar de posicao com os nos a sua frente.

Faca um programa que implemente a lista dinamica duplamente encadeada das
palavras procuradas no site de busca Ong.

Entrada
O programa tera apenas um caso de teste contendo todas as palavras procuradas 
no site. As palavras procuradas sao compostas apenas por caracteres minusculos
e tem comprimento maximo de 100 caracteres. A entrada termina com EOF.

Saida
A saida do programa consistira em varias linhas, onde em cada linha sera
impressa a palavra buscada, seguido de um espaco em branco e a quantidade de
vezes que a palavra foi procurada no site entre colchetes. Apos a impressao das
informacoes, inclusive da ultima palavra, salte uma linha.

Exemplo:

(Entrada)
Palmeiras
Corinthians
Santos
Flamengo
Fluminense
Palmeiras
Santos
Santos
Cruzeiro
Bahia
Cruzeiro
Ceara
Cruzeiro
Palmeiras
Cruzeiro
Botafogo
Ceara
Chapecoense
Chapecoense
Chapecoense
Chapecoense
Chapecoense

(Saida)
Chapecoense [5]
Cruzeiro [4]
Santos [3]
Palmeiras [3]
Ceara [2]
Corinthians [1]
Flamengo [1]
Fluminense [1]
Bahia [1]
Botafogo [1]

*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct Ite_ {
    char pal[100];
    int quant;
    struct Ite_ *ant, *prox;
} Item;

typedef struct List_ {
    Item *pri;
    Item *ult;
} Lista;

// Cria uma lista duplamente encadeada vazia e 
// devolve um ponteiro para a lista criada.
Lista *criaLista () {
    Lista *lst = malloc (sizeof (Lista));
    lst->pri = lst->ult = NULL;
    return lst;
}

// Cria um item com o dado passado por argumento copiado para o campo pal.
// O campo quant igual a 1 e os ponteiros ant e prox com valor nulo.
// Devolve um ponteiro para o item criado.
Item *criaItem (char *s) {
    Item *it = malloc (sizeof (Item));
    strcpy(it->pal, s);
    it->quant = 1;
    it->ant = it->prox = NULL;
    return it;
}

// Imprime a lista dinamica duplamente encadeada do primeiro item ate
// o ultimo.
void ranking (Lista *lst) {
    Item *p;
    for (p = lst->pri; p != NULL; p = p->prox)
        printf("%s [%d]\n", p->pal, p->quant);
}

// Devolve verdadeira se lista vazia, falso caso contrario.
bool vazia (Lista *lst) {
    return (lst->pri == NULL);
}

/*
 Neste exercicio, a parte fundamental eh a troca dos ponteiros
 de dois itens consecutivos.
 Para nao errar no ajuste dos ponteiros, eh fundamental desenhar
 um caso generico:
 
     (a)      (c)      (b)      (d)
    __ __    __ __    __ __    __ __
   |  |  |->|  |  |->|  |  |->|  |  |->
 <-|__|__|<-|__|__|<-|__|__|<-|__|__|

 No desenho acima, suponha que queremos trocar a ordem dos dois itens, 
 (c) e (b), para manter a ordem (a, b, c, d).
 
 Note que o desenho � generico, pois tanto (a) como (d) podem ser NULL.
 Assumimos apenas que (c) e (b) nao podem ser NULL.
*/
void troca (Item *c, Item *b) {
    Item *a = c->ant;
    Item *d = b->prox;
    if (a != NULL) {
        a->prox = b;
    }
    if (d != NULL) {
        d->ant = c;
    }
    b->ant = a;
    b->prox = c;
    c->ant = b;
    c->prox = d;
}

// Recebe como argumento uma lista duplamente encadeada e um novo item.
// Caso a palavra do novo item seja encontrada na lista, o item deve ser
// inserido no fim da lista.
// Caso a palavra nao seja encontrada na busca, a quantidade do item 
// repetido deve ser incrementada de uma unidade, e o item atualizado
// deve ser movido para a posicao correta de forma a manter a ordenacao.
void inserir (Lista *lst, Item *it) {
    Item *p = lst->pri;
    while (p != NULL && strcmp (p->pal, it->pal) != 0) {
        p = p->prox;
    }
    // Insere novo item no final da lista.
    if (p == NULL) {
        if (vazia (lst)) { 
            lst->pri = it;
            lst->ult = it;
        }
        else {
            p = lst->ult;
            p->prox = it;
            it->ant = p;
            lst->ult = it; // Ponteiro para o ultimo da lista.
        }
    }
    // Atualiza quantidade do item repetido e desloca sua posicao
    // para manter a ordenacao.
    else {
        free (it);
        p->quant++;
        Item *q = p->ant;
        while (q != NULL && q->quant < p->quant) {
            troca (q, p);
            q = p->ant;
        }
        if (q == NULL) {
            lst->pri = p; // Ponteiro para o primeiro da lista.
        }
    }
}

// desaloca memoria
void LiberaMemoria (Lista **lst) {
    Item *p, *q;
    q = (*lst)->pri;
    while (q != NULL) {
        p = q;
        q = q->prox;
        free (p);
    }
    free (*lst);
    *lst = NULL;
}


int main (int argc, char **argv) {
    Lista *lst = criaLista();
    char str[100];
    while (scanf("%s", str) == 1) {
        inserir (lst, criaItem (str));
    }
    ranking (lst);
    LiberaMemoria (&lst);
    
    return 0;    
}
