#include <stdio.h>
#include <stdlib.h>

typedef struct List {
    char *list;
    int length; // lenght of the list
    int amount; // Amount of elements in the list
} List;

List inicialize(int);
void insertElement(List*, char);
char removeElement(List*, char);
int searchElement(List, char);
void showList(List);

int main()
{
    int N, i;
    char op, val, deleted;
    List l;

    scanf("%d\n", &N);
    l = inicialize(N);

    while (scanf("%c\n", &op) != EOF) {
        if (op == 'I') {
            scanf("%c\n", &val);
            insertElement(&l, val);
        }
        else if (op == 'R') {
            scanf("%c\n", &val);
            deleted = removeElement(&l, val);
        }
        else if (op == 'B') {
            scanf("%c\n", &val);
            if (searchElement(l, val)) {
                printf("SIM\n");
            }
            else {
                printf("NAO\n");
            }
        }
        else if (op == 'M') {
            // scanf("\n");
            showList(l);
        }
    }

    return 0;
}

List inicialize(int N)
{
    List l;
    l.list = (char*)malloc(N*sizeof(char));
    l.length = N;
    l.amount = 0;

    return l;
}
void insertElement(List *l, char value)
{
    int pos, i;
    if (l->amount < l->length) {
        for (pos = 0; pos < l->amount && l->list[pos] <= value; pos++);
        if (l->list[pos-1] != value) {
            for (i = l->amount; i > pos; i--) {
                l->list[i] = l->list[i-1];
            }
            l->list[pos] = value;
            l->amount++;
        }
    }
    // showList(*l);
}
char removeElement(List *l, char value)
{
    int pos, i;
    char deleted;
    for (pos = 0; pos <= l->amount && l->list[pos] != value; pos++);
    if (pos <= l->amount) {
        deleted = l->list[pos];
        for (i = pos; i < l->amount; i++) {
            l->list[i] = l->list[i+1];
        }
        l->amount--;
    }
    // showList(*l);
    return deleted;
}
int searchElement(List l, char value)
{
    int i;
    for (i = 0; i < l.amount; i++) {
        if (l.list[i] == value) {
            return 1;
        }
    }

    return 0;
}
void showList(List l)
{
    int i;
    for (i = 0; i < l.amount-1; i++) {
        printf("%c ", l.list[i]);
    }
    printf("\n");
    // printf("%c\n", l.list[i]);
}