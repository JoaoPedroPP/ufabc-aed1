#include <stdio.h>
#include <stdlib.h>

typedef struct Item {
	int key;
	struct Item *next;
} Item;

typedef struct List {
	Item *begin;
} List;

List* inicializeList();
void showList(List);
Item* inicializeItem(int);
void insertItem(List*, Item*);
Item* removeItem(List*, int);
void searchItem(List, int);

int main()
{
	int value;
	char op;
	List *l = inicializeList();
	Item *removed = NULL;

	while (scanf("%c\n", &op) != EOF) {
		if (op == 'I') {
			scanf("%d\n", &value);
		//	printf("Insere %d\n", value);
			insertItem(l, inicializeItem(value));
		}
		else if (op == 'R') {
			scanf("%d\n", &value);
			//printf("Remove %d\n", value);
			removed = removeItem(l, value);
			if (removed != NULL) {
				printf("REMOVIDO: %d\n", removed->key);
				free(removed);
			}
		}
		else if (op == 'B') {
			scanf("%d\n", &value);
			//printf("Busca %d\n", value);
			searchItem(*l, value);
		}
		else if (op == 'M') {
		//	printf("lista\n");
			showList(*l);
		}
		//else {
		//	printf("ERROR\n");
		//}
	}
	
	return 0;
}


List* inicializeList()
{
	List *l = (List*)malloc(sizeof(List));
	if (l != NULL) {
		l->begin = NULL;
	}

	return l;

}

void showList(List l)
{
	Item *it = l.begin;
	if (l.begin == NULL) {
		printf("Underflow Error: Lista Vazia.\n");
	}
	else {
		while (it != NULL) {
			printf("%d%s", it->key, it->next == NULL ? "\n":" ");
			it = it->next;
		}
	}
}


Item* inicializeItem(int key)
{
	Item * it = (Item*)malloc(sizeof(Item));
	it->key = key;
	it->next = NULL;

	return it;
}


void insertItem(List *l, Item *it)
{
	Item *before = NULL;
	Item *currently = l->begin;
	//printf("Chave: %d\n", it->key);

	while (currently != NULL && currently->key <= it->key) {
		if (currently->key == it->key) {
			printf("Logic Error: Chave ja existe na Lista.\n");
			return;
		}
		else {
			before = currently;
			currently = currently->next;
		}
	}
	if (before == NULL && currently == NULL) {
		l->begin = it;
	}
	else if (before == NULL && currently != NULL) {
		l->begin = it;
		it->next = currently;
	}
	else {
		before->next = it;
		it->next = currently;
	}
	//showList(*l);

}
Item* removeItem(List *l, int key)
{
	Item *removed = NULL;
	Item *before = NULL;
	Item *currently = l->begin;

	while (currently != NULL && currently->key != key) {
		before = currently;
		currently = currently->next;
	}

	if (before == NULL && currently != NULL) {
		removed = currently;
		l->begin = currently->next;
	}

	else if (currently != NULL) {
		removed = currently;
		before->next = currently->next;
	}

	else if (before != NULL && currently == NULL) {
		printf("Logic Error: Chave inexistente na Lista.\n");
	}

	else if (before == NULL && currently == NULL) {
		printf("Underflow Error: Lista Vazia.\n");
	}
	//showList(*l);

	return removed;
}

void searchItem(List l, int key)
{
	Item *it = l.begin;

	while (it != NULL) {
		if (it->key == key) {
			printf("SIM\n");
			return;
		}
		else {
			it = it->next;
		}
	}
	if (l.begin == NULL) {
		printf("Underflow Error: Lista Vazia.\n");
	}

	else {
		printf("Logic Error: Chave inexistente na Lista.\n");
	}
}
