#include <stdio.h>
#include <stdlib.h>

typedef struct Lista
{
    int *cel;
    int tamanho;
    int quantidade;
} Lista;

Lista inicializaLista(int);
void insereLista(Lista *, int);
int removeLista(Lista *, int);
void show(Lista);


int main()
{
    int q, val, i, deleted;
    char op;
    Lista l;

    scanf("%d\n", &q);
    l = inicializaLista(q);
    for (i = 0; i < q; i++){
        scanf("%c %d\n", &op, &val);
        if (op == 'I') {
            // printf("Inserindo: %d\n", val);
            insereLista(&l, val);
        }
        else if (op == 'E') {
            // printf("Exluindo: %d\n", val);
            deleted = removeLista(&l, val);
        }
    }
    show(l);

    return 0;
}

Lista inicializaLista(int q)
{
    Lista lista;
    lista.cel = (int*)malloc(q*sizeof(int));
    lista.quantidade = 0;
    lista.tamanho = q;

    return lista;
}

void insereLista(Lista *l, int valor)
{
    int pos = 0;
    int i;
    if (l->quantidade < l->tamanho) {
        while (pos < l->quantidade && l->cel[pos] <= valor) {
            if (l->cel[pos] == valor) {
                return;
            }
            pos++;
        }

        if (pos < l->quantidade) {
            for (i = l->quantidade; i >= pos; i--) {
                l->cel[i+1] = l->cel[i];
            }
        }
        l->cel[pos] = valor;
        l->quantidade++;
    }
}

int removeLista(Lista *l, int valor)
{
    int ret, pos, i;

    if (l->quantidade > 0) {
        while (l->cel[pos] != valor && pos <= l->quantidade) {
            pos++;
        }
        if (pos <= l->quantidade) {
            ret = l->cel[pos];
            for (i = pos; i < l->quantidade; i++) {
                l->cel[i] = l->cel[i+1];
            }
            l->quantidade--;
        }
    }
    return ret;
}

void show(Lista l)
{
    int i;
    for (i = 0; i < l.quantidade; i++){
        printf("%d\n", l.cel[i]);
    }
}