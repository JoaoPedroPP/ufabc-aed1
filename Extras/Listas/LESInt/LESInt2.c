#include <stdio.h>
#include <stdlib.h>

typedef struct List {
    int *list;
    int length; // lenght of the list
    int amount; // Amount of elements in the list
} List;

List inicialize(int);
void insertElement(List*, int);
int removeElement(List*, int);
int searchElement(List, int);
void showList(List);

int main()
{
    int N, val, i, deleted;
    char op;
    List l;

    scanf("%d\n", &N);
    l = inicialize(N);

    while (scanf("%c\n", &op) != EOF) {
        if (op == 'I') {
            scanf("%d\n", &val);
            insertElement(&l, val);
        }
        else if (op == 'R') {
            scanf("%d\n", &val);
            deleted = removeElement(&l, val);
        }
        else if (op == 'B') {
            scanf("%d\n", &val);
            if (searchElement(l, val)) {
                printf("SIM\n");
            }
            else {
                printf("NAO\n");
            }
        }
        else if (op == 'M') {
            scanf("\n");
            showList(l);
        }
        else {
            printf("dont\n");
        }
    }

    return 0;
}

List inicialize(int N)
{
    List l;
    l.list = (int*)malloc(N*sizeof(int));
    l.length = N;
    l.amount = 0;

    return l;
}
void insertElement(List *l, int value)
{
    int pos, i;
    if (l->amount < l->length) {
        for (pos = 0; pos < l->amount && l->list[pos] <= value; pos++);
        if (l->list[pos-1] != value) {
            for (i = l->amount; i > pos; i--) {
                l->list[i] = l->list[i-1];
            }
            l->list[pos] = value;
            l->amount++;
        }
    }
    // showList(*l);
}
int removeElement(List *l, int value)
{
    int pos, i, deleted;
    for (pos = 0; pos <= l->amount && l->list[pos] != value; pos++);
    if (pos <= l->amount) {
        deleted = l->list[pos];
        for (i = pos; i < l->amount-1; i++) {
            l->list[i] = l->list[i+1];
        }
        l->amount--;
    }
    // showList(*l);
    return deleted;
}
int searchElement(List l, int value)
{
    int i;
    for (i = 0; i < l.amount; i++) {
        if (l.list[i] == value) {
            return 1;
        }
    }

    return 0;
}
void showList(List l)
{
    int i;
    for (i = 0; i < l.amount-1; i++) {
        printf("%d ", l.list[i]);
    }
    printf("%d\n", l.list[i]);
    // printf("\n");
}
