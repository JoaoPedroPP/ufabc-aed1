#include <stdio.h>
#include <stdlib.h>

typedef struct Item {
	int key;
	struct Item *next;
    struct Item *before;
} Item;

typedef struct List {
	Item *begin;
} List;

List* inicializeList();
void showList(List);
Item* inicializeItem(int);
void insertItem(List*, Item*);
Item* removeItem(List*, int);
void showListFT(List);

int main()
{
	int value;
	char op;
	List *l = inicializeList();
	Item *removed = NULL;

	while (scanf("%c\n", &op) != EOF) {
		if (op == 'I') {
			scanf("%d\n", &value);
			// printf("Insere %d\n", value);
			insertItem(l, inicializeItem(value));
		}
		else if (op == 'E') {
			scanf("%d\n", &value);
			printf("Remove %d\n", value);
			removed = removeItem(l, value);
			if (removed != NULL) {
				free(removed);
			}
		}
		else if (op == 'R') {
			//printf("Busca %d\n", value);
			showListFT(*l);
		}
		else if (op == 'M') {
		//	printf("lista\n");
			showList(*l);
		}
		//else {
		//	printf("ERROR\n");
		//}
	}
	
	return 0;
}


List* inicializeList()
{
	List *l = (List*)malloc(sizeof(List));
	if (l != NULL) {
		l->begin = NULL;
	}

	return l;

}

void showList(List l)
{
	Item *it = l.begin;
	while (it != NULL) {
		printf("%d%s", it->key, it->next == NULL ? "\n":" ");
		it = it->next;
	}
}


Item* inicializeItem(int key)
{
	Item * it = (Item*)malloc(sizeof(Item));
	it->key = key;
	it->next = NULL;
    it->before = NULL;

	return it;
}


void insertItem(List *l, Item *it)
{
	Item *before = NULL;
	Item *currently = l->begin;
	//printf("Chave: %d\n", it->key);

	while (currently != NULL && currently->key <= it->key) {
		if (currently->key == it->key) {
			return;
		}
		else {
			before = currently;
			currently = currently->next;
		}
	}
	if (before == NULL && currently == NULL) { // Inicio da lista
		l->begin = it;
	}
	else if (before == NULL && currently != NULL) { // Insercao do primeiro
		l->begin = it;
        currently->before = it;
		it->next = currently;
	}
	else if (before != NULL && currently != NULL) { // Caso meio/final da lista
		before->next = it;
        it->before = before;
        currently->before = it;
		it->next = currently;
	}
    else {
        before->next = it;
        it->before = before;
    }
	// showList(*l);

}
Item* removeItem(List *l, int key)
{
	Item *removed = NULL;
	Item *before = NULL;
	Item *currently = l->begin;

	while (currently != NULL && currently->key != key) {
		before = currently;
		currently = currently->next;
	}

	if (before == NULL && currently != NULL) {
		removed = currently;
        if (currently->next != NULL) {
		    l->begin = currently->next;
            currently->next->before = NULL;
        }
        else {
            l->begin = NULL;
        // printf("%d\n", key);
        }
        // currently->before = l;
	}

	else if (currently != NULL) {
		removed = currently;
		before->next = currently->next;
        currently->before = before;
	}
    else if (currently == NULL) {
        printf("x%d\n", key);
    }
	// showList(*l);

	return removed;
}

void showListFT(List l)
{
    Item *it = l.begin;
    while (it->next != NULL) {
        it = it->next;
    }

    while(it->before != NULL){
        printf("%d ", it->key);
        it = it->before;
    }
    printf("%d\n", it->key);
    
}