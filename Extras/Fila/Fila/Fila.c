#include <stdio.h>
#include <stdlib.h>

typedef struct Fila {
    int *element;
    int start;
    int end;
    int length;
} Fila;

Fila inicialize(int);
void insertElement(Fila*, int);
int removeElement(Fila*);
void showFila(Fila);

int main()
{
    int N, val, deleted;
    char op;
    Fila f;

    scanf("%d\n", &N);
    f = inicialize(N);

    while (scanf("%c\n", &op) != EOF) {
        if (op == 'E') {
            scanf("%d\n", &val);
            insertElement(&f, val);
            // printf("Incluir %d na lista\n", val);
        }
        else if (op == 'D') {
            // printf("desincluir na lista\n");
            deleted = removeElement(&f);
            printf("%d\n", deleted);
        }
    }

    return 0;
}

Fila inicialize(int N)
{
    Fila f;
    f.element = (int*)malloc(N*sizeof(int));
    f.start = -1;
    f.end = -1;
    f.length = N;

    return f;
}
void insertElement(Fila *f, int value)
{
    if (f->end < f->length) {
        if (f->start == -1) {
            f->element[0] = value;
            f->start++;
        }
        else {
            f->element[f->end+1] = value;
        }
        f->end++;
    }
    // printf("start: %d End: %d\n", f->start, f->end);
    // showFila(*f);
}
int removeElement(Fila *f)
{
    int i, deleted;
    if (f->start > -1) {
        deleted = f->element[f->start];
        f->start++;
        if (f->start == f->end) {
            f->start = -1;
            f->end = -1;
        }
        // printf("start: %d End: %d\n", f->start, f->end);
        return deleted;
    }
    // showFila(*f);
    // return deleted;
}
void showFila(Fila f)
{
    int i;
    for (i = f.start; i < f.end; i++) {
        printf("%d ", f.element[i]);
    }
    printf("\n");
}