#include <stdio.h>
#include <stdlib.h>

typedef struct Item {
    int key;
    struct Item *next;
} Item;

typedef struct Fila {
    Item *begin;
} Fila;

Fila* inicializeFila();
Item* inicializeItem(int);
void insertItem(Fila*, Item*);
void showFila(Fila);
Item* removeItem(Fila*);

int main()
{
    char op;
    int value;
    Item *removed = NULL;

    Fila *f = inicializeFila();
    while(scanf("%c\n", &op) != EOF){
        if (op == 'E') {
            scanf("%d\n", &value);
            insertItem(f, inicializeItem(value));
            // printf("Inserindo: %d\n", value);
        }
        else if (op == 'D') {
            removed = removeItem(f);
            if (removed != NULL) {
                printf("<%d>\n", removed->key);
                free(removed);
            }
            // printf("Desenfilerar\n");
        }
        else if (op == 'M') {
            // printf("Exibir\n");
            showFila(*f);
        }
    }
    
}

void showFila(Fila f)
{
    Item *it = f.begin;

    while (it != NULL) {
        printf("%d%s", it->key, it->next == NULL ? "\n":" ");
        it = it->next;
    }
}

Fila* inicializeFila()
{
    Fila *f = (Fila*)malloc(sizeof(Fila));
    f->begin = NULL;

    return f;
}

Item* inicializeItem(int key)
{
    Item *it = (Item*)malloc(sizeof(Item));
    it->key = key;
    it->next = NULL;

    return it;
}

void insertItem(Fila *f, Item *it)
{
    // Item *before = NULL;
    Item *currently = f->begin;

    if (f->begin == NULL){ // Fila vazia
        f->begin = it;
    }

    else {
        while (currently->next != NULL) {
            currently = currently->next;
        }

        currently->next = it;
    }


    // showFila(*f);
}

Item* removeItem(Fila *f)
{
    Item *removed = NULL;
    if (f->begin != NULL) {
        removed = f->begin;
        if (removed->next == NULL) {
            f->begin = NULL;
        }
        else {
            f->begin = removed->next;
        }
    }

    return removed;
}