#include <stdio.h>
#include <stdlib.h>

typedef struct Item {
    int key;
    struct Item *before;
} Item;

typedef struct Stack {
    Item *top;
} Stack;

Stack* inicializeStack();
Item* inicializeItem(int);
void showStack(Stack);
void stack(Stack*, Item*);
Item* unstack(Stack*);

int main()
{
    char op;
    int value;
    Item *removed = NULL;
    Stack *s = inicializeStack();

    while(scanf("%c\n", &op) != EOF) {
        if (op == 'E') {
            scanf("%d\n", &value);
            stack(s, inicializeItem(value));
            // printf("Empilhando: %d\n", value);
        }

        else if (op == 'D') {
            removed = unstack(s);
            if (removed != NULL) {
                printf("[%d]\n", removed->key);
                free(removed);
            }
            // printf("Desmpilhando\n");
        }

        else if (op == 'M') {
            showStack(*s);
            // printf("Mostrando\n");
        }
    }

    return 0;
}

Stack* inicializeStack()
{
    Stack *s  = (Stack*)malloc(sizeof(Stack));
    s->top = NULL;

    return s;
}

Item* inicializeItem(int key)
{
    Item *it = (Item*)malloc(sizeof(Item));
    it->key = key;
    it->before = NULL;

    return it;
}

void showStack(Stack s)
{
    Item *it = s.top;
    if (s.top != NULL) {
        while (it != NULL) {
            printf("%d%s", it->key, it->before == NULL ? "\n": " ");
            it = it->before;
        }
    }
}

void stack(Stack *s, Item *it)
{
    if (s->top == NULL) {
        s->top = it;
    }

    else
    {
        it->before = s->top;
        s->top = it;
    }
    // showStack(*s);
}

Item* unstack(Stack* s)
{
    Item *removed = NULL;
    if (s->top != NULL) {
        removed = s->top;
        if (removed->before == NULL) {
            s->top = NULL;
        }

        else {
            s->top = removed->before;
        }
    }

    return removed;
}