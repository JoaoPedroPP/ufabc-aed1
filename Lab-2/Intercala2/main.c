#include <stdio.h>
#include <stdlib.h>

void show(int*, int);
int* intercala(int*, int, int);

int main()
{
    int q1, q2, i;
    int *vi, *v;

    scanf("%d\n %d\n", &q1, &q2);

    vi = (int*)malloc((q1+q2)*sizeof(int));

    for (i = 0; i < q1 + q2; i++) {
        scanf("%d\n", (vi+i));
    }

    v = intercala(vi, q1, q2);
    show(v, q1+q2);

    free(vi);
    free(v);

    return 0;
}

void show(int *v, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        printf("%d\n", *(v+i));
    }
}

int* intercala(int *v1, int n1, int n2)
{
    int i;

    int *atual1 = v1;
    int *atual2 = v1 + n1;

    int *fim1 = (v1+n1-1);
    int *fim2 = (v1+n1+n2-1);

    int *v = (int*)malloc((n1+n2)*sizeof(int));
    for (i = 0; i < n1+n2; i++) {
        if (atual1 <= fim1 && *atual1 <= *atual2) {
            *(v+i) = *(atual1);
            atual1 = atual1+1;
        }
        else if (atual2 <= fim2 && *atual2 <= *atual1) {
            *(v+i) = *(atual2);
            atual2 = atual2+1;
        }

        else if (atual1 > fim1 && atual2 < fim2+1) {
            *(v+i) = *(atual2);
            atual2 = atual2+1;
        }

        else if (atual2 > fim2 && atual1 < fim1+1) {
            *(v+i) = *(atual1);
            atual1 = atual1+1;
        }
        else {
            printf("nope\n");
        }
    }

    return v;
}