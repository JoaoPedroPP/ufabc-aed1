#include <stdio.h>
#include <stdlib.h>

void bubbleSort();
void show(int*, int);

int main()
{
    int N, i;
    int *v = NULL;

    scanf("%d\n", &N);
    v = (int*)malloc(N*sizeof(int));

    for (i = 0; i < N; i++) {
        scanf("%d\n", v+i);
    }

    // Pinta antes de ordenar
    show(v, N);

    bubbleSort(v, N);

    free(v);

    return 0;
}

void show(int *v, int N)
{
    int i;
    for (i = 0; i < N; i++) {
        printf("%d%s", *(v+i), i == N-1 ? "\n":" ");
    }
}

void bubbleSort(int *v, int N)
{
    int i, j;
    int changes = 0;
    for (i = 0; i < N; i++) {
        for (j = 0; j < N-1; j++) {
            if (*(v+j) > *(v+j+1)) {
                int change = *(v+j);
                *(v+j) = *(v+j+1);
                *(v+j+1) = change;
                changes++;
                show(v, N);
            }
        }
    }
    show(v, N);
    printf("Trocas: %d\n", changes);
}