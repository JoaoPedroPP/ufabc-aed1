#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* intercala(char*, char*, int, int);

int main()
{
    int q1, q2;
    char *v1, *v2, *v;

    scanf("%d\n", &q1);
    v1 = (char*)malloc(q1*sizeof(char));
    scanf("%s\n", v1);
    // *(v1+q1) = '\0';
    // printf("%s\n", v1);

    scanf("%d\n", &q2);
    v2 = (char*)malloc(q2*sizeof(char));
    scanf("%s\n", v2);
    // *(v2+q2) = '\0';
    // printf("%s\n", v2);

    v = intercala(v1, v2, q1, q2);
    printf("%s\n", v);
    // show(v, q1+q2);

    free(v1);
    free(v2);
    free(v);

    return 0;
}

char* intercala(char *v1, char *v2, int n1, int n2)
{
    int i;

    char *atual1 = v1;
    char *atual2 = v2;

    char *fim1 = (v1+n1-1);
    char *fim2 = (v2+n2-1);

    char *v = (char*)malloc((n1+n2+1)*sizeof(char));
    // *(v+n1+n2) = '\0';
    for (i = 0; i < n1+n2; i++) {
        if (*atual1 != '\0' && *atual1 <= *atual2) {
            *(v+i) = *(atual1);
            atual1 = atual1+1;
        }
        else if (*atual2 != '\0' && *atual2 <= *atual1) {
            *(v+i) = *(atual2);
            atual2 = atual2+1;
        }

        else if (*atual1 == '\0' && atual2 <= fim2) {
            *(v+i) = *(atual2);
            atual2 = atual2+1;
        }

        else if (*atual2 == '\0' && atual1 <= fim1) {
            *(v+i) = *(atual1);
            atual1 = atual1+1;
        }
        else {
            printf("nope\n");
        }
        *(v+i+1) = '\0';
        // printf("%s\n", v);
    }

    return v;
}