#include <stdio.h>

int verify(int[], int);

int main()
{
    int N, i, n;
    while (scanf("%d", &N) != EOF) {
        int v[N];
        for (i = 0; i < N; i++) {
            scanf("%d\n", &v[i]);
            printf("posicao: %d/%d, dado: %d\n", i, N, v[i]);
        }
        if (N > 0) {
            printf("%s\n", verify(v, N) > 0 ? "S":"N");
        }
        else if (N == 0) {
            printf("S\n");
        }
    }

    return 0;
}

int verify(int v[], int N)
{
    int i;
    int crescente = 0;
    int decrescente = 0;

    // verifica ordenacao decrescente
    for (i = 0; i < N; i++) {
        if (v[i] >= v[i+1]) {
            decrescente = 1;
        }
        else {
            decrescente = 0;
             i = N;
        }
    }

    // verifica ordenacao crescente
    for (i = 0; i < N-1; i++) {
        if (v[i] <= v[i+1]) {
            crescente = 1;
        }
        else {
            crescente = 0;
            i = N;
        }
    }

    return decrescente+crescente;
}