#include <stdio.h>

int verifyUp(int[], int);
int verifyDown(int[], int);

int main()
{
    int N, i, n;
    while (scanf("%d\n", &N) != EOF) {
        int v[N];
        for (i = 0; i < N; i++) {
            scanf("%d\n", &v[i]);
        }
        if (N > 0) {
            printf("%s\n", verifyUp(v, N-1) + verifyDown(v, N-1) > 0 ? "S":"N");
        }
        else if (N == 0) {
            printf("S\n");
        }
    }

    return 0;
}

int verifyUp(int v[], int N)
{
    if (N == 0) {
        return 1;
    }
    else {
        if (v[N] >= v[N-1]) {
            return verifyUp(v, N-1);
        }
        else {
            return 0;
        }
    }
}

int verifyDown(int *v, int N)
{
    if (N == 0) {
        return 1;
    }
    else {
        if (v[N] <= v[N-1]) {
            return verifyDown(v, N-1);
        }
        else {
            return 0;
        }
    }
}