#include <stdio.h>
#include <stdlib.h>

void show(int*, int);
int verify(int*, int);

int main()
{
    int N, i, n;
    int *v;
    while (scanf("%d\n", &N) != EOF) {
        v = (int*)malloc(N*sizeof(int));
        for (i = 0; i < N; i++) {
            scanf("%d\n", v+i);
        }
        if (N > 0) {
            printf("%s\n", verify(v, N) > 0 ? "S":"N");
        }
        else if (N == 0) {
            printf("S\n");
        }
        free(v);
    }

    return 0;
}

void show(int *v, int N)
{
    int i;
    for (i = 0; i < N; i++) {
        printf("%d%s", *(v+1), i == N-1 ? "\n":" ");
    }
}

int verify(int *v, int N)
{
    int i;
    int crescente = 0;
    int decrescente = 0;

    // verifica ordenacao decrescente
    for (i = 0; i < N-1; i++) {
        if (*(v+i) >= *(v+i+1)) {
            decrescente = 1;
        }
        else {
            decrescente = 0;
             i = N;
        }
    }

    // verifica ordenacao crescente
    for (i = 0; i < N-1; i++) {
        if (*(v+i) <= *(v+i+1)) {
            crescente = 1;
        }
        else {
            crescente = 0;
            i = N;
        }
    }

    return decrescente+crescente;
}