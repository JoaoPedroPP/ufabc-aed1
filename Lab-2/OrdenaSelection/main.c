#include <stdio.h>
#include <stdlib.h>

void show(int*, int);
int selectionSort(int*, int);

int main()
{
    int N, i, changes;
    int *v = NULL;
    
    scanf("%d\n", &N);
    v = (int*)malloc(N*sizeof(int));
    
    for (i = 0; i < N; i++) {
        scanf("%d\n", v+i);
    }

    show(v, N);
    changes = selectionSort(v, N);
    show(v, N);
    printf("%d\n", changes);

    free(v);

    return 0;
}

void show(int *v, int N)
{
    int i;
    for (i = 0; i < N; i++) {
        printf("%d%s", *(v+i), i == N-1 ? "\n":" ");
    }
}

int selectionSort(int *v, int N)
{
    int i, j, value, min, change;
    int changes = 0;
    int status = 0;

    for (i = 0; i < N-1; i++) {
        value = *(v+i);
        min = i;
        for (j = i; j < N; j++) {
            if (*(v+j) < *(v+min)) {
                min = j;
                status = 1;
                changes++;
            }
        }
        change = *(v+i);
        *(v+i) = *(v+min);
        *(v+min) = change;
        show(v, N);
    }
    return changes;
}