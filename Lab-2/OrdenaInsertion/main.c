#include <stdio.h>
#include <stdlib.h>

void insertSort();
void show(int*, int);
int reduce(int, int);

int main()
{
    int N, i;
    int *v = NULL;

    scanf("%d\n", &N);
    v = (int*)malloc(N*sizeof(int));

    for (i = 0; i < N; i++) {
        scanf("%d\n", v+i);
    }

    // Pinta antes de ordenar
    show(v, N);

    insertSort(v, N);

    free(v);

    return 0;
}

void show(int *v, int N)
{
    int i;
    for (i = 0; i < N; i++) {
        printf("%d%s", *(v+i), i == N-1 ? "\n":" ");
    }
}

void insertSort(int *v, int N)
{
    int i, j, value;
    int changes = 0;
    int status = 0;
    for (i = 1; i < N; i++) {
        value = *(v+i);
        j = i - 1;
        while (j >= 0 && *(v+j) > value) {
            *(v+j+1) = *(v+j);
            j--;
            changes++;
            status = 1;
            *(v+j+1) = value;
            show(v, N);
        }
    }
    show(v, N);
    printf("Trocas:%d\n", changes);
    // printf("Trocas: %d\n", reduce(N-1, 0));

    if (!changes) {
        printf("MELHOR CASO\n");
    }
    else if (changes == reduce(N-1, 0)) {
        printf("PIOR CASO\n");
    }
    else {
        printf("CASO ALEATORIO\n");
    }
}

int reduce(int N, int sum)
{
    if (N == 0) {
        return N + sum;
    }
    else {
        return sum + reduce(N-1, N);
    }
}