#include <stdio.h>
#include <stdlib.h>

int verifyUp(int*, int);

int main()
{
    int N, i, n;
    int *v;
    while (scanf("%d\n", &N) != EOF) {
        v = (int*)malloc(N*sizeof(int));
        for (i = 0; i < N; i++) {
            scanf("%d\n", v+i);
            // printf("posicao: %d/%d, dado: %d\n", i, N, *(v+i));
        }
        if (N > 0) {
            printf("%s\n", verifyUp(v, N-1) > 0 ? "S":"N");
        }
        else if (N == 0) {
            printf("S\n");
        }
        free(v);
    }

    return 0;
}

int verifyUp(int *v, int N)
{
    if (N == 0) {
        return 1;
    }
    else {
        // printf("Up: %d\n", *(v+N));
        if (*(v+N) >= *(v+N-1)) {
            return verifyUp(v, N-1);
        }
        else {
            return 0;
        }
    }
}