#include <stdio.h>
#include <stdlib.h>

void show(int*, int);
int* intercala(int*, int*, int, int);

int main()
{
    int q1, q2, i;
    int *v1, *v2, *v;

    scanf("%d\n %d\n", &q1, &q2);
    // scanf("%d\n", &q2);

    v1 = (int*)malloc(q1*sizeof(int));
    v2 = (int*)malloc(q2*sizeof(int));

    // for (i = 0; i < q1 + q2; i++) { // Precisa arrumar isso aqui
    //     if (i < q1) {
    //         scanf("%d\n", (v1+i));
    //     }
    //     else {
    //         int diff = i - q1;
    //         // printf("%d\n", diff);
    //         scanf("%d\n", (v2+i-q1));
    //     }
    // }
    for (i = 0; i < q1; i++) {
        scanf("%d\n", (v1+i));
    }
    for (i = 0; i < q2; i++) {
        scanf("%d\n", (v2+i));
    }

    v = intercala(v1, v2, q1, q2);
    show(v, q1+q2);

    free(v1);
    free(v2);
    free(v);

    return 0;
}

void show(int *v, int n)
{
    int i;
    for (i = 0; i < n; i++) {
        printf("%d\n", *(v+i));
    }
}

int* intercala(int *v1, int *v2, int n1, int n2)
{
    int i;

    int *atual1 = v1;
    int *atual2 = v2;

    int *fim1 = (v1+n1-1);
    int *fim2 = (v2+n2-1);

    int *v = (int*)malloc((n1+n2)*sizeof(int));
    for (i = 0; i < n1+n2; i++) {
        if (atual1 <= fim1 && *atual1 <= *atual2) {
            *(v+i) = *(atual1);
            atual1 = atual1+1;
        }
        else if (atual2 <= fim2 && *atual2 <= *atual1) {
            *(v+i) = *(atual2);
            atual2 = atual2+1;
        }

        else if (atual1 > fim1 && atual2 < fim2+1) {
            *(v+i) = *(atual2);
            atual2 = atual2+1;
        }

        else if (atual2 > fim2 && atual1 < fim1+1) {
            *(v+i) = *(atual1);
            atual1 = atual1+1;
        }
        else {
            printf("nope\n");
        }
    }

    return v;
}